# Authors: Ryan Frost, Nathan Sanchez
# Emails: rfrost@nd.edu, nsanche3@nd.edu
#
# This is the Makefile for the IBM Preternship Project

# G++ is the compiler for C++
PP := g++

# CFLAGS are the compiler flags for when we compile C code
FLAGS := -O0 -g -Wall -Wextra -Wconversion -Wshadow -pedantic -Werror
CXXFLAGS := -m64 -std=c++11 $(FLAGS)

# Variables for Folders
IBM := ~/ibm-preternship-project

# Command: make testfile
testObjs :=  $(IBM)/testfile.o $(IBM)/INCIDENT.o

testfile: $(testObjs)
	$(PP) $(CXXFLAGS) -o $(IBM)/testfile $(testObjs)

testfile.o: $(IBM)/testfile.cpp $(IBM)/INCIDENT.h
	$(PP) $(CXXFLAGS) -c $(IBM)/testfile.cpp
	
INCIDENT.o: $(IBM)/INCIDENT.h $(IBM)/INCIDENT.cpp
	$(PP) $(CXXFLAGS) -c $(IBM)/INCIDENT.cpp

	
# Make all
all: testfile

# Make clean
clean:
	rm -rf *.o testfile
	