/**********************************************
* File: INCIDENT.cpp
* Authors: Ryan Frost, Nathan Sanchez
* Emails: rfrost@nd.edu, nsanche3@nd.edu
*
* This is the .cpp file for the class INCIDENT containing the method definitions
**********************************************/

#include "INCIDENT.h"

// typedefs for strings and string vectors to save space
typedef std::string STR;
typedef std::vector < std::string > VEC;

// Single class constructor
INCIDENT::INCIDENT(STR company_nameIn, STR dateIn, STR descriptionIn) :
company_name(company_nameIn) , date(dateIn) , description(descriptionIn)
{
	// Variables used for impact and urgency calculations
	int totalUrg = 0;
	int totalImpact = 0;
	int urgCount = 0;
	int impactCount = 0;
	int tempUrg;
	int tempImpact;

	/* scans the description character by character and concatenates each character into 
	a string 'tempWord' until a space is reached to create a vector of all words in the
	description. */
	STR tempWord = "";
	for(auto x : descriptionIn){
		if(x == ' '){
			word_vec.push_back(tempWord);
			tempWord = "";
		}
		else{
			tempWord = tempWord + x;
		}
	}
	

	STR word;
	int lenWordVec = (int)word_vec.size(); // length of the word vector typecast to an int
	// Loops through the wordUrgency/wordImpact functions for each word in .TXT file
	for(int i = 0; i < lenWordVec; i++){
		word = word_vec.at(i);
		tempUrg = wordUrgency(word);
		// 1, 2, 3 corresp. to Low, Medium, and High urgency, respectively
		if(tempUrg == 1 || tempUrg == 2 || tempUrg == 3){
			totalUrg = totalUrg + tempUrg;
			urgCount = urgCount + 1;
			// Inserts the keyword into key_word_vec
			key_word_vec.push_back(word);
		}
		tempImpact = wordImpact(word);
		// 1, 2, 3 corresp. to Low, Medium, and High urgency, respectively
		if(tempImpact == 1 || tempImpact == 2 || tempImpact == 3){
			totalImpact = totalImpact + tempImpact;
			impactCount = impactCount + 1;
			// Inserts the keyword into key_word_vec
			key_word_vec.push_back(word);
		}
	}
	
	/* typecasts to a double, calculates average urgency for the issue by dividing the
	total urgency by the number of 'urgency' words and then rounds it to the nearest whole
	number: 1, 2, or 3. Repeated for issue impact as well */
	double avgUrg = (double)totalUrg / (double)urgCount;
	double avgImpact = (double)totalImpact / (double)impactCount;
	double roundedUrg = round(avgUrg);
	double roundedImpact = round(avgImpact);
	
	// Assigns private member urgency and impact based on rounded value
	if(roundedUrg == 1){
		urgency = "Low";
	}
	else if(roundedUrg == 2){
		urgency = "Medium";
	}
	else if(roundedUrg == 3){
		urgency = "High";
	}

	if(roundedImpact == 1){
		impact = "Low";
	}
	else if(roundedImpact == 2){
		impact = "Medium";
	}
	else if(roundedImpact == 3){
		impact = "High";
	}

	// Calculates the priority code from the rounded urgency and impact w/ this func. call
	int priority_codeIn = calcCode(roundedUrg, roundedImpact);

	// Assigns the ITIL values in a vector for priority code 1
	VEC code1;
	code1.push_back("Critical");
	code1.push_back("Immediate");
	code1.push_back("1 hour");

	// Assigns the ITIL values in a vector for priority code 2
	VEC code2;
	code2.push_back("High");
	code2.push_back("10 minutes");
	code2.push_back("8 hours");

	// Assigns the ITIL values in a vector for priority code 3
	VEC code3;
	code3.push_back("Medium");
	code3.push_back("1 hour");
	code3.push_back("24 hours");

	// Assigns the ITIL values in a vector for priority code 4
	VEC code4;
	code4.push_back("Low");
	code4.push_back("4 hours");
	code4.push_back("48 hours");

	// Assigns the ITIL values in a vector for priority code
	VEC code5;
	code5.push_back("Planning");
	code5.push_back("1 day");
	code5.push_back("1 week");
	
	// Assigns the above defined ITIL values based on the produced priority code
	if(priority_codeIn == 1){
		priority_code = priority_codeIn;
		adj = code1.at(0);
		resp_time = code1.at(1);
		resol_time = code1.at(2);
	}
	else if(priority_codeIn == 2){
		priority_code = priority_codeIn;
		adj = code2.at(0);
		resp_time = code2.at(1);
		resol_time = code2.at(2);
	}
	else if(priority_codeIn == 3){
		priority_code = priority_codeIn;
		adj = code3.at(0);
		resp_time = code3.at(1);
		resol_time = code3.at(2);
	}
	else if(priority_codeIn == 4){
		priority_code = priority_codeIn;
		adj = code4.at(0);
		resp_time = code4.at(1);
		resol_time = code4.at(2);
	}
	else if(priority_codeIn == 5){
		priority_code = priority_codeIn;
		adj = code5.at(0);
		resp_time = code5.at(1);
		resol_time = code5.at(2);
	}
}


// Get method definitions, returns corresponding private members

/********************************************
* Function Name  : INCIDENT::getCompName
* Pre-conditions : none
* Post-conditions: STR
* 
* Returns the private member data for 'company_name' 
********************************************/
STR INCIDENT::getCompName() const{
	return company_name;
}

/********************************************
* Function Name  : INCIDENT::getDate
* Pre-conditions : none
* Post-conditions: STR
* Returns the private member data for 'date' 
********************************************/
STR INCIDENT::getDate() const{
	return date;
}

/********************************************
* Function Name  : INCIDENT::getDescrip
* Pre-conditions : none
* Post-conditions: STR
* 
* Returns the private member data for 'description' 
********************************************/
STR INCIDENT::getDescrip() const{
	return description;
}

/********************************************
* Function Name  : INCIDENT::getUrgency
* Pre-conditions : none
* Post-conditions: STR
* 
* Returns the private member data for 'urgency' 
********************************************/
STR INCIDENT::getUrgency() const{
	return urgency;
}

/********************************************
* Function Name  : INCIDENT::getImpact
* Pre-conditions : none
* Post-conditions: STR
* 
* Returns the private member data for 'impact' 
********************************************/
STR INCIDENT::getImpact() const{
	return impact;
}

/********************************************
* Function Name  : INCIDENT::getKeyWordVec
* Pre-conditions : none
* Post-conditions: VEC
* 
* Returns a vector private member, 'key_word_vec' 
********************************************/
VEC INCIDENT::getKeyWordVec() const{
	return key_word_vec;
}

/********************************************
* Function Name  : INCIDENT::getPriorityCode
* Pre-conditions : none
* Post-conditions: int
* 
* Returns the private member data for 'priority_code' 
********************************************/
int INCIDENT::getPriorityCode() const{
	return priority_code;
}

/********************************************
* Function Name  : INCIDENT::getAdj
* Pre-conditions : none
* Post-conditions: STR
* 
* Returns the private member data for 'adj' — the issue descriptor 
********************************************/
STR INCIDENT::getAdj() const{
	return adj;
}

/********************************************
* Function Name  : INCIDENT::getRespTime
* Pre-conditions : none
* Post-conditions: STR
* 
* Returns the private member data for 'resp_time' — the target response time 
********************************************/
STR INCIDENT::getRespTime() const{
	return resp_time;
}

/********************************************
* Function Name  : INCIDENT::getResolTime
* Pre-conditions : none
* Post-conditions: STR
* 
* Returns the private member data for 'resol_time' — the target resolution time 
********************************************/
STR INCIDENT::getResolTime() const{
	return resol_time;
}


// ITIL classification methods

/********************************************
* Function Name  : wordUrgency
* Pre-conditions : std::stringword
* Post-conditions: double
*
* Assigns an urgency level to relevant keywords. 1 is "Low", 2 is "Medium", and 3 is
* "High". 
********************************************/
int INCIDENT::wordUrgency(STR word){
	int urgValue = 0; // this value is incremented depending on urgency
	if(word == "crucial"){
		urgValue = urgValue + 3;
	}
	else if(word == "essential"){
		urgValue = urgValue + 2;
	}
	else if(word == "important"){
		urgValue = urgValue + 1;
	}
	else if(word == "critical"){
		urgValue = urgValue + 3;
	}
	else if(word == "vital"){
		urgValue = urgValue + 2;
	}
	else if(word == "serious"){
		urgValue = urgValue + 1;
	}
	else if(word == "imperative"){
		urgValue = urgValue + 2;
	}
	else if(word == "rush"){
		urgValue = urgValue + 1;
	}
	else if(word == "hurry"){
		urgValue = urgValue + 1;
	}
	else if(word == "demanding"){
		urgValue = urgValue + 2;
	}
	else if(word == "compel"){
		urgValue = urgValue + 2;
	}
	else if(word == "now"){
		urgValue = urgValue + 2;
	}
	else if(word == "functional"){
		urgValue = urgValue + 1;
	}
	else if(word == "slow"){
		urgValue = urgValue + 1;
	}
	else if(word == "inoperative"){
		urgValue = urgValue + 2;
	}
	else if(word == "unusable"){
		urgValue = urgValue + 3;
	}
	else if(word == "nonfunctional"){
		urgValue = urgValue + 2;
	}
	else if(word == "restricting"){
		urgValue = urgValue + 2;
	}
	else if(word == "limiting"){
		urgValue = urgValue + 1;
	}
	else if(word == "impeding"){
		urgValue = urgValue + 2;
	}
	else if(word == "broken"){
		urgValue = urgValue + 3;
	}
	else if(word == "inhibiting"){
		urgValue = urgValue + 2;
	}
	else if(word == "quickly"){
		urgValue = urgValue + 2;
	}
	else if(word == "crucial"){
		urgValue = urgValue + 3;
	}
	else if(word == "essential"){
		urgValue = urgValue + 2;
	}	
	else if(word == "important"){
		urgValue = urgValue + 1;
	}
	else if(word == "critical"){
		urgValue = urgValue + 3;
	}
	else if(word == "vital"){
		urgValue = urgValue + 2;
	}
	else if(word == "serious"){
		urgValue = urgValue + 1;
	}
	else if(word == "imperative"){
		urgValue = urgValue + 2;
	}
	else if(word == "rush"){
		urgValue = urgValue + 1;
	}
	else if(word == "hurry"){
		urgValue = urgValue + 1;
	}
	else if(word == "demanding"){
		urgValue = urgValue + 2;
	}
	else if(word == "compel"){
		urgValue = urgValue + 2;
	}
	else if(word == "now"){
		urgValue = urgValue + 2;
	}
	else if(word == "functional"){
		urgValue = urgValue + 1;
	}
	else if(word == "slow"){
		urgValue = urgValue + 1;
	}
	else if(word == "inoperative"){
		urgValue = urgValue + 2;
	}
	else if(word == "unusable"){
		urgValue = urgValue + 3;
	}
	else if(word == "nonfunctional"){
		urgValue = urgValue + 2;
	}
	else if(word == "restricting"){
		urgValue = urgValue + 2;
	}
	else if(word == "limiting"){
		urgValue = urgValue + 1;
	}
	else if(word == "impeding"){
		urgValue = urgValue + 2;
	}
	else if(word == "broken"){
		urgValue = urgValue + 3;
	}
	else if(word == "inhibiting"){
		urgValue = urgValue + 2;
	}
	else if(word == "quickly"){
		urgValue = urgValue + 2;
	}
	else{
		urgValue = urgValue + 0;
	}
	// Returns the Urgency Value of the keyword
	return urgValue;
}

/********************************************
* Function Name  : wordImpact
* Pre-conditions : std::stringword
* Post-conditions: double
*
* Assigns an impact level to relevant keywords. 1 is "Low", 2 is "Medium", and 3 is
* "High". 
********************************************/
int INCIDENT::wordImpact(STR word){

	int impactValue = 0; // this value is incremented depending on the impact from 0.

	if(word == "drastic"){
		impactValue = impactValue + 3;
	}
	else if(word == "necessary"){
		impactValue = impactValue + 2;
	}
	else if(word == "broken"){
		impactValue = impactValue + 2;
	}
	else if(word == "critical"){
		impactValue = impactValue + 3;
	}
	else if(word == "vital"){
		impactValue = impactValue + 2;
	}
	else if(word == "serious"){
		impactValue = impactValue + 1;
	}
	else if(word == "imperative"){
		impactValue = impactValue + 2;
	}
	else if(word == "process"){
		impactValue = impactValue + 3;
	}
	else if(word == "unable"){
		impactValue = impactValue + 2;
	}
	else if(word == "change"){
		impactValue = impactValue + 1;
	}
	else if(word == "overwhelming"){
		impactValue = impactValue + 2;
	}
	else if(word == "demanding"){
		impactValue = impactValue + 1;
	}
	else if(word == "modify"){
		impactValue = impactValue + 2;
	}
	else if(word == "modifications"){
		impactValue = impactValue + 2;
	}
	else if(word == "modify"){
		impactValue = impactValue + 1;
	}
	else if(word == "slow"){
		impactValue = impactValue + 1;
	}
	else if(word == "hardware"){
		impactValue = impactValue + 2;
	}
	else if(word == "software"){
		impactValue = impactValue + 2;
	}
	else if(word == "down"){
		impactValue = impactValue + 3;
	}
	else if(word == "malfunctioning"){
		impactValue = impactValue + 2;
	}
	else if(word == "unresponsive"){
		impactValue = impactValue + 3;
	}
	else if(word == "broken"){
		impactValue = impactValue + 1;
	}
	else if(word == "damaging"){
		impactValue = impactValue + 2;
	}
	else if(word == "faulty"){
		impactValue = impactValue + 3;
	}
	else if(word == "inactive"){
		impactValue = impactValue + 3;
	}
	else if(word == "drastic"){
		impactValue = impactValue + 3;
	}
	else if(word == "necessary"){
		impactValue = impactValue + 2;
	}	
	else if(word == "broken"){
		impactValue = impactValue + 2;
	}
	else if(word == "critical"){
		impactValue = impactValue + 3;
	}
	else if(word == "vital"){
		impactValue = impactValue + 2;
	}
	else if(word == "serious"){
		impactValue = impactValue + 1;
	}
	else if(word == "imperative"){
		impactValue = impactValue + 2;
	}
	else if(word == "process"){
		impactValue = impactValue + 3;
	}
	else if(word == "unable"){
		impactValue = impactValue + 2;
	}
	else if(word == "change"){
		impactValue = impactValue + 1;
	}
	else if(word == "overwhelming"){
		impactValue = impactValue + 2;
	}
	else if(word == "demanding"){
		impactValue = impactValue + 1;
	}
	else if(word == "modify"){
		impactValue = impactValue + 2;
	}
	else if(word == "modifications"){
		impactValue = impactValue + 2;
	}
	else if(word == "modify"){
		impactValue = impactValue + 1;
	}
	else if(word == "slow"){
		impactValue = impactValue + 1;
	}
	else if(word == "hardware"){
		impactValue = impactValue + 2;
	}
	else if(word == "software"){
		impactValue = impactValue + 2;
	}
	else if(word == "down"){
		impactValue = impactValue + 3;
	}
	else if(word == "malfunctioning"){
		impactValue = impactValue + 2;
	}
	else if(word == "unresponsive"){
		impactValue = impactValue + 3;
	}
	else if(word == "broken"){
		impactValue = impactValue + 1;
	}
	else if(word == "damaging"){
		impactValue = impactValue + 2;
	}
	else if(word == "faulty"){
		impactValue = impactValue + 3;
	}
	else if(word == "inactive"){
		impactValue = impactValue + 3;
	}
	else{
		impactValue = impactValue + 0;
	}
	// Returns the impact Value of the keyword
	return impactValue;
}

/********************************************
* Function Name  : calcCode
* Pre-conditions : std::stringurgency, std::stringimpact
* Post-conditions: int
*
* This function uses the averaged urgency and impact values
* to assign a priority code to the issue, and sets the string values
* for impact and urgency in the class.
********************************************/
int INCIDENT::calcCode(double urgencyIn, double impactIn){
	// Depending on the urgency and impact value, it assings
	// A priority code depending on the severity of the problem
	int priorityCode;

	if(urgencyIn == 3 && impactIn == 3){
		priorityCode = 1;
	}
	else if(urgencyIn == 3 && impactIn == 2){
		priorityCode = 2;
	}
	else if(urgencyIn == 3 && impactIn == 1){
		priorityCode = 3;
	}
	else if(urgencyIn == 2 && impactIn == 3){
		priorityCode = 2;
	}
	else if(urgencyIn == 2 && impactIn == 2){
		priorityCode = 3;
	}
	else if(urgencyIn == 2 && impactIn == 1){
		priorityCode = 4;
	}
	else if(urgencyIn == 1 && impactIn == 3){
		priorityCode = 3;
	}
	else if(urgencyIn == 1 && impactIn == 2){
		priorityCode = 4;
	}
	else if(urgencyIn == 1 && impactIn == 1){
		priorityCode = 5;
	}
	return priorityCode;
}
