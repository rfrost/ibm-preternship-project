The University of Notre Dame | 11/21/2019 | The systems are currently down. They are limiting the amount of work that can be done. It is critical that these issues
 get resolved quickly. It is important that there is a rush since the servers are critical in our operations.|
IBM | 11/25/2019 | There is an essential and demanding need for our servers to go back to normal. It is vital and critical that these issues are resolved in a hurry.
It is a serious problem. |
Target | 11/20/2019 | We are having issues with our servers. We are unable to process transactions. During this time of the year, There are many people coming to the store.
We need this issue resolved as quickly as possible. These systems are critical to our day to day operations. They are impeding the work that can be done and we are also losing money as a result.
These systems are imperative to us. |
Nike | 11/23/2019 | The systems are in critical condition.There are software and hardware issues that need to be fixed. The issues are drastic and the servers are inactive. It is crucial that they run.|
Boeing | 11/24/2019 | Our systems are unusable and slowed down by a network crash, getting our systems back up is critical. |
