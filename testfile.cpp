/**********************************************
* File: testfile.cpp
* Authors: Nathan Sanchez, Ryan Frost
* Emails: nsanche3@nd.edu, rfrost@nd.edu
* 
* This is the main file for the program.
**********************************************/

#include "INCIDENT.h"

// Typedefs for strings and string vectors to save space
typedef std::string STR;
typedef std::vector < std::string > VEC;

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* 
* This is the main driver function for the program. 
********************************************/
int main(){
	
	// For reading in the input .txt file
	STR word;
	STR file_name = "TestInput.txt";
	std::fstream fin(file_name, std::fstream::in);
	// Creates a vector to store each subsequent issue, of type INCIDENT
	std::vector < INCIDENT > data;
	
	/* Sets the date, description, and company in memory to a string "0" and then uses
	this to fill in the inputs for the constructor */
	STR date = "0";
	STR company = "0";
	STR desc = "0";
	
	// Reads the .TXT file line by line until it reaches '|' character
	while(getline(fin, word, '|')){
		if(company == "0"){
			company = word;
		}else if(date == "0"){
			date = word;
		}else if(desc == "0"){
			desc = word;
			// Call constructor + sets up information into a vector
			INCIDENT c1(company, date, desc);
			// pushes back the 'data' vector
			data.push_back(c1);
			// resets the date, company, and description to string "0" for next call
			date = "0";
			company = "0";
			desc = "0";
		}
	}
	
	// Bubble sort to swap data vector elements according to priority code
	int lenData = (int)data.size();
	for(int g = 0; g < lenData - 1; g++){
		for(int h = 0; h < lenData - g - 1; h++){
			if(data.at(h).getPriorityCode() > data.at(h + 1).getPriorityCode()){
				// performing the swap using a temporary INCIDENT variable
				INCIDENT tempIncident = data.at(h);
				data.at(h) = data.at(h + 1);
				data.at(h + 1) = tempIncident;
			}
		}
	}

	/* Creates a file pointer 'fileOut' to output to a file named "ReportedIssues.txt"
	and uses it to write directly to it */
	std::ofstream fileOut("ReportedIssues.txt");
	fileOut << "Issues Report" << std::endl;
	fileOut << "–––––––––––––––––––––––––––––––––––––––––" << std::endl;
	// Prints each incident and its relevant information by looping through data vector
	int i = 0;
	for(i = 0; i < lenData; i++){
		// to correct for a reading in error for the first incident not adding an endline
		fileOut << "Incident " << i + 1 << std::endl;
		fileOut << data.at(i).getCompName() << std::endl;
		fileOut << "Date:	" << data.at(i).getDate() << std::endl;
		fileOut << "Issue Description:	" << data.at(i).getDescrip() << std::endl;
		fileOut << "Key Words Analyzed:   ";
		
		// loops through the keyword vector to print out all of the elements
		VEC tempKeyWordVec = data.at(i).getKeyWordVec();
		int lenKeyWordVec = (int)tempKeyWordVec.size();
		for(int k = 1; k < lenKeyWordVec; k++){
				fileOut << tempKeyWordVec.at(k) << ", ";
		}
		fileOut << std::endl;
		fileOut << "Urgency Level:	" << data.at(i).getUrgency() << std::endl;
		fileOut << "Impact Level:	" << data.at(i).getImpact() << std::endl;
		fileOut << "Priority Code:	" << data.at(i).getPriorityCode() << std::endl;
		fileOut << "Issue Descriptor:	" << data.at(i).getAdj() << std::endl;
		fileOut << "Target Response Time:	" << data.at(i).getRespTime() << std::endl;
		fileOut << "Target Resolution Time:	   " << data.at(i).getResolTime() << std::endl;
		fileOut << std::endl;
		fileOut << "–––––––––––––––––––––––––––––––––––––––––" << std::endl;
	}
	// end of the main file once all of incidents are printed out
	return 0;
}
	