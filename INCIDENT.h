/**********************************************
* File: INCIDENT.h
* Authors: Ryan Frost, Nathan Sanchez
* Emails: rfrost@nd.edu, nsanche3@nd.edu
* 
* This is the header file for the class INCIDENT containing the method declarations
**********************************************/

#ifndef INCIDENT_H
#define INCIDENT_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <math.h>

typedef std::string STR;
typedef std::vector < std::string > VEC;

class INCIDENT{

	private:
	
		STR company_name;   // company name
		STR date;           // date
		STR description;    // issue description
		STR urgency;		// ITIL assigned urgency
		STR impact;			// ITIL assigned impact
		VEC word_vec;		// vector of all words in issue description
		VEC key_word_vec;	// vector of ITIL-significant words from the issue description
		int priority_code;  // calculated ITIL priority code for the issue
		STR adj;			// descriptor of the severity of the issue (given priority code)
		STR resp_time;		// the target response time for the issue (given priority code)
		STR resol_time;		// the target resolution time for the issue (given priority code)		
		
	
	public:

		INCIDENT(STR company_nameIn, STR dateIn, STR descriptionIn);
		
		STR getCompName() const;
		STR getDate() const;
		STR getDescrip() const;
		STR getUrgency() const;
		STR getImpact() const;
		VEC getKeyWordVec() const;
		int getPriorityCode() const;
		STR getAdj() const;
		STR getRespTime() const;
		STR getResolTime() const;
				
		int wordUrgency(STR word);
		int wordImpact(STR word);
		int calcCode(double urgency, double impact);

};

#endif
